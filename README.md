# Raytrace

I recently hit some another raytracer vid on YT and I realized I never did my
own, and this is something I find being interesting piece of math, so I decided
to try something.

The RT would be losely based on
[Ray Tracing in One Weekend](https://raytracing.github.io/books/RayTracingInOneWeekend.html),
but written in Rust, with as much GPU support as possible. Will see where I would
finish my journey.

## Logging

Logging is realized with [tracing](https://docs.rs/tracing/latest/tracing/) crate,
and can be easly configured setting the `RUST_LOG` env variable as for
[env_logger](https://crates.io/crates/env_logger).

## Backend

By default gpu backend is Vulkan, it can be changed with `WGPU_BACKEND` env.

## Other config

Any other configuration can be aligned by envs, or program arguments. Check
`--help` for valid arguments. As crate is using `dotenv`, it is possible to
easly store config in the `.env` file.
