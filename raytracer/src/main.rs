use anyhow::Error;
use dotenv::dotenv;
use structopt::StructOpt;
use tracing::{error, info, trace};
use winit::dpi::PhysicalSize;
use winit::event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::WindowBuilder;

mod video;

#[derive(Debug, StructOpt)]
#[structopt(name = "raytracer", about = "Toy raytracer")]
struct Opt {
    #[structopt(short, long, env = "WIDTH")]
    width: u32,
    #[structopt(short, long, env = "HEIGHT")]
    height: u32,
}

fn run(config: Opt) -> Result<(), Error> {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_inner_size(PhysicalSize::new(config.width, config.height))
        .with_resizable(false)
        .with_title("Raytracer")
        .build(&event_loop)?;

    info!("Window initialized");

    let window_id = window.id();
    let mut vid = pollster::block_on(video::Video::new(window))?;

    info!("Video initialized");

    event_loop.run(move |ev, _, flow| match &ev {
        Event::WindowEvent {
            event,
            window_id: wid,
        } if *wid == window_id => match event {
            WindowEvent::CloseRequested
            | WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        state: ElementState::Pressed,
                        virtual_keycode: Some(VirtualKeyCode::Escape),
                        ..
                    },
                ..
            } => {
                trace!(?ev, "Escape pressed");
                *flow = ControlFlow::Exit
            }
            _ => {}
        },
        Event::RedrawRequested(_) => match vid.render() {
            Ok(_) => {}
            Err(err) => match err.downcast::<wgpu::SurfaceError>() {
                Ok(wgpu::SurfaceError::Lost) => vid.configure(),
                Ok(wgpu::SurfaceError::OutOfMemory) => *flow = ControlFlow::Exit,
                Ok(err) => error!(?err, "Error, occurred"),
                Err(err) => error!(?err, "Error, occurred"),
            },
        },
        Event::MainEventsCleared => vid.window().request_redraw(),
        _ => {}
    });
}

fn main() -> Result<(), Error> {
    dotenv().ok();
    tracing_subscriber::fmt::init();

    let config = Opt::from_args();
    info!(?config, "Starting app");

    if let Err(err) = run(config) {
        error!(?err, "Error occurred");
    }

    Ok(())
}
