use anyhow::{anyhow, Error};
use std::time::{Duration, SystemTime};
use tracing::trace;
use wgpu::include_spirv;
use winit::window::Window;

mod bindings;
mod viewport;

pub struct Video {
    window: Window,
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    config: wgpu::SurfaceConfiguration,
    pipeline: wgpu::RenderPipeline,

    vp_binding: wgpu::BindGroup,

    frames: u128,
    last_fps_updated: SystemTime,
}

impl Video {
    pub async fn new(window: Window) -> Result<Self, Error> {
        let instance = wgpu::Instance::new(wgpu::Backends::all());
        // It is safe - window is owned by `Video` and it will not go out of
        // scope while `surface` is need to be alife.
        let surface = unsafe { instance.create_surface(&window) };
        let adapter = Self::create_adapter(&instance, &surface).await?;
        let (device, queue) = Self::request_device(&adapter).await?;

        let shader = device.create_shader_module(&include_spirv!(env!("shader.spv")));
        let config = Self::pipeline_config(&window, &surface, &adapter)?;

        let vp_buffer = viewport::Viewport::new(
            2.0,
            window.inner_size().width as f32 / (window.inner_size().height as f32),
            1.0,
        )
        .create_buffer(&device);
        let (vp_layout, vp_binding) = Self::create_uniform_binding_group(
            &device,
            bindings::VP_BINDING,
            vp_buffer.as_entire_binding(),
            "VP layout",
            "VP group",
        );

        let pipeline_layout = Self::pipeline_layout(&device, &[&vp_layout]);
        let pipeline = Self::build_pipeline(&device, &pipeline_layout, &shader, &config);

        let vid = Self {
            window,
            surface,
            device,
            queue,
            config,
            pipeline,

            vp_binding,

            frames: 0,
            last_fps_updated: SystemTime::now(),
        };

        vid.configure();

        Ok(vid)
    }

    pub fn configure(&self) {
        self.surface.configure(&self.device, &self.config);
    }

    pub fn render(&mut self) -> Result<(), Error> {
        let output = self.surface.get_current_texture()?;
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Raytracer"),
            });

        {
            let mut pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Raytracer pass"),
                color_attachments: &[wgpu::RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.0,
                            g: 0.0,
                            b: 0.0,
                            a: 1.0,
                        }),
                        store: true,
                    },
                }],
                depth_stencil_attachment: None,
            });

            pass.set_pipeline(&self.pipeline);
            pass.set_bind_group(bindings::VP_BINDING_GROUP, &self.vp_binding, &[]);
            pass.draw(0..4, 0..1);
        }

        self.queue.submit(std::iter::once(encoder.finish()));
        output.present();

        self.frames += 1;
        let now = SystemTime::now();
        let elapsed = SystemTime::now().duration_since(self.last_fps_updated)?;
        if elapsed > Duration::from_secs(5) {
            let fps = self.frames / elapsed.as_secs() as u128;
            let avg_duration = elapsed.as_nanos() as u128 / self.frames;
            trace!(?fps, ?avg_duration, "FPS trace");

            self.frames = 0;
            self.last_fps_updated = now;
        }

        Ok(())
    }

    pub fn window(&self) -> &Window {
        &self.window
    }

    async fn create_adapter(
        instance: &wgpu::Instance,
        surface: &wgpu::Surface,
    ) -> Result<wgpu::Adapter, Error> {
        instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .ok_or_else(|| anyhow!("No valid adapter"))
    }

    async fn request_device(adapter: &wgpu::Adapter) -> Result<(wgpu::Device, wgpu::Queue), Error> {
        adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    features: wgpu::Features::empty(),
                    limits: wgpu::Limits::default(),
                    label: None,
                },
                None,
            )
            .await
            .map_err(Error::from)
    }

    fn create_uniform_binding_group(
        device: &wgpu::Device,
        binding: u32,
        resource: wgpu::BindingResource,
        label_layout: &str,
        label_group: &str,
    ) -> (wgpu::BindGroupLayout, wgpu::BindGroup) {
        let layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: Some(label_layout),
            entries: &[wgpu::BindGroupLayoutEntry {
                binding,
                visibility: wgpu::ShaderStages::FRAGMENT,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: None,
                },
                count: None,
            }],
        });

        let group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some(label_group),
            layout: &layout,
            entries: &[wgpu::BindGroupEntry { binding, resource }],
        });

        (layout, group)
    }

    fn pipeline_config(
        window: &Window,
        surface: &wgpu::Surface,
        adapter: &wgpu::Adapter,
    ) -> Result<wgpu::SurfaceConfiguration, Error> {
        let size = window.inner_size();

        Ok(wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: surface
                .get_preferred_format(&adapter)
                .ok_or_else(|| anyhow!("No preferred color format"))?,
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Immediate,
        })
    }

    fn pipeline_layout(
        device: &wgpu::Device,
        bind_group_layouts: &[&wgpu::BindGroupLayout],
    ) -> wgpu::PipelineLayout {
        device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("Pipeline layout"),
            bind_group_layouts,
            push_constant_ranges: &[],
        })
    }

    fn build_pipeline(
        device: &wgpu::Device,
        layout: &wgpu::PipelineLayout,
        shader: &wgpu::ShaderModule,
        config: &wgpu::SurfaceConfiguration,
    ) -> wgpu::RenderPipeline {
        device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("Pipeline"),
            layout: Some(layout),
            vertex: wgpu::VertexState {
                module: shader,
                entry_point: "main_vs",
                buffers: &[],
            },
            fragment: Some(wgpu::FragmentState {
                module: shader,
                entry_point: "main_fs",
                targets: &[wgpu::ColorTargetState {
                    format: config.format,
                    blend: Some(wgpu::BlendState::REPLACE),
                    write_mask: wgpu::ColorWrites::ALL,
                }],
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleStrip,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: Some(wgpu::Face::Back),
                polygon_mode: wgpu::PolygonMode::Fill,
                clamp_depth: false,
                conservative: false,
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
        })
    }
}
