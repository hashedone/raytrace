use bytemuck::{Pod, Zeroable};
use wgpu::util::DeviceExt;

#[repr(C)]
#[derive(Debug, Clone, Copy, Pod, Zeroable)]
pub struct Viewport {
    view: [f32; 4],
    focal: f32,
}

impl Viewport {
    pub fn new(width: f32, ratio: f32, focal: f32) -> Self {
        let height = width / ratio;
        Self {
            view: [-width / 2.0, width / 2.0, -height / 2.0, height / 2.0],
            focal,
        }
    }

    pub fn create_buffer(self, device: &wgpu::Device) -> wgpu::Buffer {
        device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Viewport buffer"),
            contents: bytemuck::cast_slice(&[self]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        })
    }
}
