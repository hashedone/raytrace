use spirv_builder::{MetadataPrintout, SpirvBuilder};

fn main() {
    SpirvBuilder::new(
        concat!(env!("CARGO_MANIFEST_DIR"), "/../shader"),
        "spirv-unknown-vulkan1.2",
    )
    .print_metadata(MetadataPrintout::Full)
    .build()
    .unwrap();
}
