#![cfg_attr(
    target_arch = "spirv",
    no_std,
    feature(register_attr, lang_items),
    register_attr(spirv)
)]
#![deny(warnings)]
#![allow(dead_code)]

#[cfg(not(target_arch = "spirv"))]
use spirv_std::macros::spirv;

use spirv_std::glam::{vec2, vec3, vec4, Vec2, Vec4};

mod ray;
mod utils;

use ray::Ray;
use utils::interpolate;

pub struct Viewport {
    view: Vec4,
    focal: f32,
}

#[spirv(fragment)]
pub fn main_fs(
    #[spirv(uniform, descriptor_set = 0, binding = 0)] vp: &Viewport,
    coords: Vec2,
    output: &mut Vec4,
) {
    let [l, r, b, t] = vp.view.to_array();
    let x = interpolate(l, r, coords.x);
    let y = interpolate(b, t, coords.y);
    let p = vec3(x, y, vp.focal);

    let orig = vec3(0.0, 0.0, 0.0);
    let ray = Ray::new(orig, p - orig);

    *output = ray.color();
}

#[spirv(vertex)]
pub fn main_vs(
    #[spirv(vertex_index)] vert_id: u32,
    coords: &mut Vec2,
    #[spirv(position, invariant)] pos: &mut Vec4,
) {
    let x = vert_id & 1;
    let y = vert_id & 2;

    *coords = vec2(x as f32, (y >> 1) as f32);
    *pos = vec4((x as f32 - 0.5) * 2.0, y as f32 - 1.0, 0.0, 1.0);
}
