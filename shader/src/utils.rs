use core::ops::{Add, Mul, Sub};
use spirv_std::num_traits::*;

pub fn interpolate<Scale, T>(from: T, to: T, scale: Scale) -> T
where
    Scale: One + Sub<Output = Scale> + Copy,
    T: Mul<Scale, Output = T> + Add<Output = T>,
{
    from * (Scale::one() - scale) + to * scale
}
