use crate::utils::interpolate;
use spirv_std::glam::{vec4, Vec3, Vec4};

#[derive(Default)]
pub struct Ray {
    orig: Vec3,
    dir: Vec3,
}

impl Ray {
    pub fn new(orig: Vec3, dir: Vec3) -> Self {
        Self {
            orig: orig.normalize(),
            dir,
        }
    }

    pub fn at(&self, t: f32) -> Vec3 {
        self.orig + self.dir * t
    }

    pub fn color(&self) -> Vec4 {
        let t = 0.5 * (self.dir.y as f32 + 1.0);
        interpolate(vec4(1.0, 1.0, 1.0, 1.0), vec4(0.5, 0.7, 1.0, 1.0), t)
    }
}
